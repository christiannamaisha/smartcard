<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Student
 * @package App\Models
 * @version May 11, 2022, 11:14 pm UTC
 *
 * @property string $groupe_sanguin
 * @property string $allergie
 * @property string $maladie_recurrente
 * @property unsignedInteger $user_id
 * @property unsignedInteger $classe_id
 */
class Student extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'students';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'groupe_sanguin',
        'allergie',
        'maladie_recurrente',
        'user_id',
        'classe_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'groupe_sanguin' => 'string',
        'allergie' => 'string',
        'maladie_recurrente' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required'
    ];

    public static $rules_updated = [
    ];
    
}
