<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Cashier
 * @package App\Models
 * @version May 11, 2022, 11:29 pm UTC
 *
 * @property unsignedInteger $user_id
 */
class Cashier extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'cashiers';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required'
    ];

    public static $rules_updated = [
    ];

    
}
