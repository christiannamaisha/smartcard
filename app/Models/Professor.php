<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Professor
 * @package App\Models
 * @version May 12, 2022, 12:37 am UTC
 *
 * @property unsignedInteger $user_id
 */
class Professor extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'professors';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required'
    ];

    public static $rules_updated = [
    ];

    
}
