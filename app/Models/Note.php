<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Note
 * @package App\Models
 * @version May 12, 2022, 12:36 am UTC
 *
 * @property unsignedInteger $course_id
 * @property unsignedInteger $student_id
 * @property string $valeur
 */
class Note extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'notes';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'course_id',
        'etudiant_id',
        'valeur'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'valeur' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'course_id' => 'required',
        'etudiant_id' => 'required',
        'valeur' => 'required'
    ];

    
}
