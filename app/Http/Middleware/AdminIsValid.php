<?php

namespace App\Http\Middleware;

use Auth;

use Closure;

class AdminIsValid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
            if(Auth::guest())
                return(redirect(route('welcome')));
            else if(!Auth::user()->isAdmin)
                return(redirect(route('welcome')));

        return $next($request);
    }
}
