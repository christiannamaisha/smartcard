<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateVigilRequest;
use App\Http\Requests\UpdateVigilRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Vigil;
use App\Models\User;
use Illuminate\Http\Request;
use Flash;
use Response;
use DB;


class VigilController extends AppBaseController
{
    /**
     * Display a listing of the Vigil.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Vigil $vigils */
        $vigils = DB::table('vigils')
        ->select('users.*')
        ->join('users','vigils.user_id','users.id')
        ->get();

        return view('vigils.index')
            ->with('vigils', $vigils);
    }

    /**
     * Show the form for creating a new Vigil.
     *
     * @return Response
     */
    public function create()
    {
        return view('vigils.create');
    }

    /**
     * Store a newly created Vigil in storage.
     *
     * @param CreateVigilRequest $request
     *
     * @return Response
     */
    public function store(CreateVigilRequest $request)
    {
        Flash::success('What are you doing .. ? 😏');

        return redirect(route('vigils.index'));
    }

    /**
     * Display the specified Vigil.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Vigil $vigil */
        $vigil = DB::table('vigils')
        ->select('users.*')
        ->join('users','vigils.user_id','users.id')
        ->where('users.id',$id)
        ->first();

        if (empty($vigil) ) {
            Flash::error('Vigil not found');

            return redirect(route('vigils.index'));
        }

        return view('vigils.show')->with('vigil', $vigil);
    }

    /**
     * Show the form for editing the specified Vigil.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Vigil $vigil */
        $vigil = DB::table('vigils')
        ->select('users.*')
        ->join('users','vigils.user_id','users.id')
        ->where('users.id',$id)
        ->first();
        
        if (empty($vigil)) {
            Flash::error('Vigil not found');

            return redirect(route('vigils.index'));
        }

        return view('vigils.edit')->with('vigil', $vigil);
    }

    /**
     * Update the specified Vigil in storage.
     *
     * @param int $id
     * @param UpdateVigilRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVigilRequest $request)
    {
        $user = User::find($id);
        /** @var Vigil $vigil */
        $vigil = DB::table('vigils')
        ->select('users.*')
        ->join('users','vigils.user_id','users.id')
        ->where('users.id',$id)
        ->first();

        if (empty($vigil)) {
            Flash::error('Vigil not found');

            return redirect(route('vigils.index'));
        }

        $user->fill($request->all());
        $user->save();

        Flash::success('Vigil updated successfully.');

        return redirect(route('vigils.index'));
    }

    /**
     * Remove the specified Vigil from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Vigil $vigil */
        $vigil = DB::table('vigils')
        ->select('vigils.*')
        ->join('users','vigils.user_id','users.id')
        ->where('users.id',$id)
        ->first();

        if (empty($vigil)) {
            Flash::error('Vigil not found');

            return redirect(route('vigils.index'));
        }

        $user->delete();
        DB::table('vigils')
            ->where('vigils.id', $vigil->id)
            ->update(['vigils.deleted_at' => now() ]);
        Flash::success('Vigil deleted successfully.');

        return redirect(route('vigils.index'));
    }
}
