<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Models\Classe;
use App\Models\User;
use Illuminate\Http\Request;
use Flash;
use Response;
use DB;
use Illuminate\Support\Facades\Crypt;


class QrcodeController extends Controller
{
    public function index()
    {
        /** @var Student $students */
        $students = DB::table('students')
        ->select('students.id', 'users.nom', 'users.prenom')
        ->join('users','students.user_id','users.id')
        ->get();

        return view('qrcode.index')
            ->with('students', $students);
    }

    public function read($data)
    {
        $id = intval(Crypt::decryptString($data)); 

        /** @var Student $students */
        $student = DB::table('students')
        ->select('users.*', 'students.groupe_sanguin', 'students.allergie', 'students.maladie_recurrente', 'students.classe_id')
        ->join('users','students.user_id','users.id')
        ->where('students.id', $id)
        ->first();
        
        $user = $student->id;

        /** @var Payments $payments */
        $payments = DB::table('payments')
        ->select('payments.month')
        ->join('users','payments.student_id','users.id')
        ->where('users.id', $user)
        ->get();

        return view('qrcode.parse')
                ->with('student', $student)
                ->with('payments', $payments);
    }
}
