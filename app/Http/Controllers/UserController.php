<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Models\Student;
use App\Models\Professor;
use App\Models\Cashier;
use App\Models\Vigil;
use App\Models\User;

use Flash;

use Illuminate\Support\Str;

class UserController extends Controller
{
    public function store(Request $request)
    {
        $input = $request->all();

        if($request->hasfile('photo'))
        {
            $filePath = 'users';
            $file = $request->file('photo');
            $extension = $file->getClientOriginalExtension(); 
            $input['photo'] = Str::random(20). '.' . $extension;
            $file->storeAs($filePath, $input['photo'], 's3');            
        }

        $input['password'] = bcrypt($input['password']);

        if($input['profile'] == "student"){
            $input['user_id'] = User::create($input)->id;
            Student::create($input);
            Flash::success('Successfully registered as a student');
            return redirect(route('students.index'));
        }
        else if ($input['profile'] == "cashier"){
            $input['user_id'] = User::create($input)->id;
            Cashier::create($input);
            Flash::success('Successfully registered as a cashier');
            return redirect(route('cashiers.index'));
        }
        else if ($input['profile'] == "vigil"){
            $input['user_id'] = User::create($input)->id;
            Vigil::create($input);
            Flash::success('Successfully registered as a vigil');
            return redirect(route('vigils.index'));
        }
        else if ($input['profile'] == "professor"){
            $input['user_id'] = User::create($input)->id;
            Professor::create($input);
            Flash::success('Successfully registered as a professor');
            return redirect(route('professors.index'));
        }
        
        return redirect(route('home'));
    }
}
