<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateStudentRequest;
use App\Http\Requests\UpdateStudentRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Student;
use App\Models\Classe;
use App\Models\User;
use Illuminate\Http\Request;
use Flash;
use Response;
use DB;

use Illuminate\Support\Str;


class StudentController extends AppBaseController
{
    /**
     * Display a listing of the Student.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Student $students */
        $students = DB::table('students')
        ->select('users.*','students.id as child','students.groupe_sanguin', 'students.allergie', 'students.maladie_recurrente', 'students.classe_id' )
        ->join('users','students.user_id','users.id')
        ->get();

        return view('students.index')
            ->with('students', $students);
    }

    /**
     * Show the form for creating a new Student.
     *
     * @return Response
     */
    public function create()
    {
        $classes = Classe::all()->pluck('libelle', 'id');
        return view('students.create')->with('classes', $classes);
        ;
    }

    /**
     * Store a newly created Student in storage.
     *
     * @param CreateStudentRequest $request
     *
     * @return Response
     */
    public function store(CreateStudentRequest $request)
    {

        Flash::success('What are you doing .. ? 😏');

        return redirect(route('students.index'));
    }

    /**
     * Display the specified Student.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Student $student */
        $student = DB::table('students')
        ->select('users.*','students.id as child','students.groupe_sanguin', 'students.allergie', 'students.maladie_recurrente', 'students.classe_id')
        ->join('users','students.user_id','users.id')
        ->where('users.id',$id)
        ->first();

        if (empty($student)) {
            Flash::error('Student not found');

            return redirect(route('students.index'));
        }

        return view('students.show')->with('student', $student);
    }

    /**
     * Show the form for editing the specified Student.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $classes = Classe::all()->pluck('libelle', 'id');
        
        /** @var Student $student */
        $student = DB::table('students')
        ->select('users.*','students.id as child','students.groupe_sanguin', 'students.allergie', 'students.maladie_recurrente', 'students.classe_id')
        ->join('users','students.user_id','users.id')
        ->where('users.id',$id)
        ->first();
        
        
        if (empty($student)) {
            Flash::error('Student not found');

            return redirect(route('students.index'));
        }

        return view('students.edit')->with('student', $student)->with('classes', $classes);
    }

    /**
     * Update the specified Student in storage.
     *
     * @param int $id
     * @param UpdateStudentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStudentRequest $request)
    {
        $input = $request->all();

        $user = User::find($id);
        /** @var Student $student */
        $student_data = DB::table('students')
        ->select('students.id as id', 'students.groupe_sanguin', 'students.allergie', 'students.maladie_recurrente', 'students.classe_id')
        ->join('users','students.user_id','users.id')
        ->where('users.id',$id)
        ->first();

        $student = Student::find($student_data->id);

        if (empty($student)) {
            Flash::error('Student not found');

            return redirect(route('students.index'));
        }

        if($request->hasfile('photo'))
        {
            $filePath = 'users';
            $file = $request->file('photo');
            $extension = $file->getClientOriginalExtension(); 
            $input['photo'] = Str::random(20). '.' . $extension;
            $file->storeAs($filePath, $input['photo'], 's3');            
        }

        $user->fill($input);
        $student->fill($request->all());

        $user->save();
        $student->save();

        Flash::success('Student updated successfully.');

        return redirect(route('students.index'));
    }

    /**
     * Remove the specified Student from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        /** @var Student $student */
        $student = DB::table('students')
        ->select('students.*')
        ->join('users','students.user_id','users.id')
        ->where('users.id',$id)
        ->first();

        if (empty($student)) {
            Flash::error('Student not found');

            return redirect(route('students.index'));
        }

        $user->delete();
        DB::table('students')
        ->where('students.id', $student->id)
        ->update(['students.deleted_at' => now() ]);

        Flash::success('Student deleted successfully.');

        return redirect(route('students.index'));
    }

    
}
