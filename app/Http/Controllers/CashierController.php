<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCashierRequest;
use App\Http\Requests\UpdateCashierRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Cashier;
use App\Models\User;
use Illuminate\Http\Request;
use Flash;
use Response;
use DB;


class CashierController extends AppBaseController
{
    /**
     * Display a listing of the Cashier.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Cashier $cashiers */
        $cashiers = DB::table('cashiers')
        ->select('users.*')
        ->join('users','cashiers.user_id','users.id')
        ->get();

        return view('cashiers.index')
            ->with('cashiers', $cashiers);
    }

    /**
     * Show the form for creating a new Cashier.
     *
     * @return Response
     */
    public function create()
    {
        return view('cashiers.create');
    }

    /**
     * Store a newly created Cashier in storage.
     *
     * @param CreateCashierRequest $request
     *
     * @return Response
     */
    public function store(CreateCashierRequest $request)
    {
        $input = $request->all();

        /** @var Cashier $cashier */
        $cashier = Cashier::create($input);

        Flash::success('Cashier saved successfully.');

        return redirect(route('cashiers.index'));
    }

    /**
     * Display the specified Cashier.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Cashier $cashier */
        $cashier = DB::table('cashiers')
        ->select('users.*')
        ->join('users','cashiers.user_id','users.id')
        ->where('users.id',$id)
        ->first();

        if (empty($cashier)) {
            Flash::error('Cashier not found');

            return redirect(route('cashiers.index'));
        }

        return view('cashiers.show')->with('cashier', $cashier);
    }

    /**
     * Show the form for editing the specified Cashier.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Cashier $cashier */
        $cashier = DB::table('cashiers')
        ->select('users.*')
        ->join('users','cashiers.user_id','users.id')
        ->where('users.id',$id)
        ->first();

        if (empty($cashier)) {
            Flash::error('Cashier not found');

            return redirect(route('cashiers.index'));
        }

        return view('cashiers.edit')->with('cashier', $cashier);
    }

    /**
     * Update the specified Cashier in storage.
     *
     * @param int $id
     * @param UpdateCashierRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCashierRequest $request)
    {
        $user = User::find($id);
        /** @var Cashier $cashier */
        $cashier = DB::table('cashiers')
        ->select('users.*')
        ->join('users','cashiers.user_id','users.id')
        ->where('users.id',$id)
        ->first();

        if (empty($cashier)) {
            Flash::error('Cashier not found');

            return redirect(route('cashiers.index'));
        }

        $user->fill($request->all());
        $user->save();

        Flash::success('Cashier updated successfully.');

        return redirect(route('cashiers.index'));
    }

    /**
     * Remove the specified Cashier from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        /** @var Cashier $cashier */
        $cashier = DB::table('cashiers')
        ->select('cashiers.*')
        ->join('users','cashiers.user_id','users.id')
        ->where('users.id',$id)
        ->first();

        if (empty($cashier)) {
            Flash::error('Cashier not found');
            return redirect(route('cashiers.index'));
        }

        $user->delete();
        DB::table('cashiers')
            ->where('cashiers.id', $cashier->id)
            ->update(['cashiers.deleted_at' => now() ]);

        Flash::success('Cashier deleted successfully.');

        return redirect(route('cashiers.index'));
    }
}
