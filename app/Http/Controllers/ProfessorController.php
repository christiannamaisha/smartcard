<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProfessorRequest;
use App\Http\Requests\UpdateProfessorRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Professor;
use App\Models\User;
use Illuminate\Http\Request;
use Flash;
use Response;
use DB;


class ProfessorController extends AppBaseController
{
    /**
     * Display a listing of the Professor.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Professor $professors */
        $professors = DB::table('professors')
        ->select('users.*')
        ->join('users','professors.user_id','users.id')
        ->get();

        return view('professors.index')
            ->with('professors', $professors);
    }

    /**
     * Show the form for creating a new Professor.
     *
     * @return Response
     */
    public function create()
    {
        return view('professors.create');
    }

    /**
     * Store a newly created Professor in storage.
     *
     * @param CreateProfessorRequest $request
     *
     * @return Response
     */
    public function store(CreateProfessorRequest $request)
    {
        $input = $request->all();

        /** @var Professor $professor */
        $professor = Professor::create($input);

        Flash::success('Professor saved successfully.');

        return redirect(route('professors.index'));
    }

    /**
     * Display the specified Professor.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Professor $professor */
        $professor = DB::table('professors')
        ->select('users.*')
        ->join('users','professors.user_id','users.id')
        ->where('users.id',$id)
        ->first();

        if (empty($professor)) {
            Flash::error('Professor not found');

            return redirect(route('professors.index'));
        }

        return view('professors.show')->with('professor', $professor);
    }

    /**
     * Show the form for editing the specified Professor.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Professor $professor */
        $professor = DB::table('professors')
        ->select('users.*')
        ->join('users','professors.user_id','users.id')
        ->where('users.id',$id)
        ->first();

        if (empty($professor)) {
            Flash::error('Professor not found');

            return redirect(route('professors.index'));
        }

        return view('professors.edit')->with('professor', $professor);
    }

    /**
     * Update the specified Professor in storage.
     *
     * @param int $id
     * @param UpdateProfessorRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProfessorRequest $request)
    {
        $user = User::find($id);
        /** @var Professor $professor */
        $professor = DB::table('professors')
        ->select('users.*')
        ->join('users','professors.user_id','users.id')
        ->where('users.id',$id)
        ->first();

        if (empty($professor)) {
            Flash::error('Professor not found');

            return redirect(route('professors.index'));
        }

        $user->fill($request->all());
        $user->save();

        Flash::success('Professor updated successfully.');

        return redirect(route('professors.index'));
    }

    /**
     * Remove the specified Professor from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        /** @var Professor $professor */
        $professor = DB::table('professors')
        ->select('professors.*')
        ->join('users','professors.user_id','users.id')
        ->where('users.id',$id)
        ->first();

        if (empty($professor)) {
            Flash::error('Professor not found');

            return redirect(route('professors.index'));
        }
        
        $user->delete();
        DB::table('professors')
            ->where('professors.id', $professor->id)
            ->update(['professors.deleted_at' => now() ]);

        Flash::success('Professor deleted successfully.');

        return redirect(route('professors.index'));
    }
}
