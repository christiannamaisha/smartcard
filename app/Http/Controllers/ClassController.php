<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateClassRequest;
use App\Http\Requests\UpdateClassRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Classe;
use Illuminate\Http\Request;
use Flash;
use Response;

use DB;

class ClassController extends AppBaseController
{
    /**
     * Display a listing of the Class.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Classe $classes */
        $classes = Classe::all();

        return view('classes.index')
            ->with('classes', $classes);
    }

    /**
     * Show the form for creating a new Class.
     *
     * @return Response
     */
    public function create()
    {
        return view('classes.create');
    }

    /**
     * Store a newly created Class in storage.
     *
     * @param CreateClassRequest $request
     *
     * @return Response
     */
    public function store(CreateClassRequest $request)
    {
        $input = $request->all();

        /** @var Class $class */
        $class = Classe::create($input);

        Flash::success('Class saved successfully.');

        return redirect(route('classes.index'));
    }

    /**
     * Display the specified Class.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Classe $class */
        $class = Classe::find($id);

        $students = DB::table('students')
        ->select('users.nom', 'users.prenom', 'users.numero')
        ->join('users','students.user_id','users.id')
        ->where('classe_id', $class->id)
        ->get();


        if (empty($class)) {
            Flash::error('Class not found');

            return redirect(route('classes.index'));
        }

        return view('classes.show')->with('class', $class)->with('students', $students);
    }

    /**
     * Show the form for editing the specified Class.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Classe $class */
        $class = Classe::find($id);

        if (empty($class)) {
            Flash::error('Class not found');

            return redirect(route('classes.index'));
        }

        return view('classes.edit')->with('class', $class);
    }

    /**
     * Update the specified Class in storage.
     *
     * @param int $id
     * @param UpdateClassRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateClassRequest $request)
    {
        /** @var Classe $class */
        $class = Classe::find($id);

        if (empty($class)) {
            Flash::error('Class not found');

            return redirect(route('classes.index'));
        }

        $class->fill($request->all());
        $class->save();

        Flash::success('Class updated successfully.');

        return redirect(route('classes.index'));
    }

    /**
     * Remove the specified Class from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Classe $class */
        $class = Classe::find($id);

        if (empty($class)) {
            Flash::error('Class not found');

            return redirect(route('classes.index'));
        }

        $class->delete();

        Flash::success('Class deleted successfully.');

        return redirect(route('classes.index'));
    }
}
