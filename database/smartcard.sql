-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 14, 2022 at 10:09 PM
-- Server version: 5.7.32
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smartcard`
--

-- --------------------------------------------------------

--
-- Table structure for table `cashiers`
--

CREATE TABLE `cashiers` (
  `id` int(11) NOT NULL,
  `user_id` int(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cashiers`
--

INSERT INTO `cashiers` (`id`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 21, '2022-05-14 18:54:56', '2022-05-14 18:54:56', '2022-05-14 18:58:16'),
(2, 22, '2022-05-14 18:59:12', '2022-05-14 18:59:12', NULL),
(3, 26, '2022-05-14 21:53:28', '2022-05-14 21:53:28', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `id` int(11) NOT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`id`, `libelle`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'L1IAGE', '2022-05-09 13:05:47', NULL, NULL),
(2, 'L2IAGE', '2022-05-09 13:05:47', NULL, NULL),
(3, 'L3IAGE', '2022-05-09 13:05:47', NULL, NULL),
(4, 'L1TTL', '2022-05-09 13:05:47', NULL, NULL),
(5, 'L2TTL', '2022-05-09 13:07:00', NULL, NULL),
(6, 'L3TTL', '2022-05-09 13:07:00', NULL, NULL),
(7, 'L1GLRS', '2022-05-09 13:07:00', NULL, NULL),
(8, 'L2GLRS', '2022-05-09 13:07:00', NULL, NULL),
(9, 'L3GLRS', '2022-05-09 13:08:29', NULL, NULL),
(10, 'L1MAE', '2022-05-09 13:08:39', NULL, NULL),
(11, 'L2MAE', '2022-05-09 13:08:49', NULL, NULL),
(12, 'L3MAE', '2022-05-09 13:08:56', NULL, NULL),
(13, 'BOND\'INNOV', '2022-05-14 21:51:26', '2022-05-14 21:52:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `professor_id` int(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `libelle`, `professor_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'L.E.A.R.N Laravel from scratch', 23, '2022-05-14 21:24:10', '2022-05-14 21:24:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE `notes` (
  `id` int(25) NOT NULL,
  `course_id` int(255) NOT NULL,
  `etudiant_id` int(255) NOT NULL,
  `valeur` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `professors`
--

CREATE TABLE `professors` (
  `id` int(11) NOT NULL,
  `user_id` int(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `professors`
--

INSERT INTO `professors` (`id`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(6, 23, '2022-05-14 19:01:58', '2022-05-14 19:01:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(25) NOT NULL,
  `groupe_sanguin` varchar(255) DEFAULT NULL,
  `allergie` varchar(255) DEFAULT NULL,
  `maladie_recurrente` varchar(255) DEFAULT NULL,
  `user_id` int(255) NOT NULL,
  `classe_id` tinytext,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `groupe_sanguin`, `allergie`, `maladie_recurrente`, `user_id`, `classe_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'O+', 'Pollen', 'Rhume', 18, '3', '2022-05-14 17:59:30', '2022-05-14 17:59:30', NULL),
(2, 'O+', 'Pollen', 'Rhume', 19, '3', '2022-05-14 17:59:35', '2022-05-14 17:59:35', '2022-05-14 18:33:15');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `isAdmin` int(11) NOT NULL DEFAULT '0',
  `profile` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `date_naissance` date DEFAULT NULL,
  `numero` varchar(255) DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nom`, `prenom`, `isAdmin`, `profile`, `email`, `password`, `photo`, `date_naissance`, `numero`, `email_verified_at`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin', NULL, 1, '', 'admin@smartcard.nl', '$2y$10$LxP5zHAKvccqIz5WAHTX/egWOnuBHvefIGgLbictXrJ4V4L49Ns7W', NULL, NULL, NULL, NULL, '2022-05-13 23:12:47', '2022-05-13 23:12:47', NULL),
(15, 'Moctou', 'Fall', 0, 'student', 'moctou.fall@slash.fr', '$2y$10$onIfewyWPIAbsSnYoQmiuuiAHN48jj8FZzsWJvEu0A0m9Cy1W1fD2', NULL, '2022-05-02', '+221 77 085 45 56', NULL, '2022-05-14 17:55:30', '2022-05-14 17:55:30', NULL),
(20, 'Rolande', 'Belvia', 0, 'cashier', 'rolande.belvia@illimitis.sn', '$2y$10$iZL8lW5gc.3mehXUHY.Q1.rLGpTyPkzHa4amuHiEayr4hlf7.XZDe', NULL, '2022-05-02', '+221 77 000 00 00', NULL, '2022-05-14 18:53:55', '2022-05-14 18:53:55', NULL),
(23, 'Moustapha', 'Nder', 0, 'professor', 'moustapha.n@ism.sn', '$2y$10$qxNoMNOFiIPGtD3XXACEp.pyzMJkMRfrDBEfkBUUJ3tOdVaHbe4CG', NULL, '2022-05-10', '+221 77 638 69 61', NULL, '2022-05-14 19:01:58', '2022-05-14 19:01:58', NULL),
(24, 'Philippe', 'Etchebest', 0, 'vigil', 'philippe.e@bodyguard.fr', '$2y$10$3BMgc2FUtnvCDiYf1mqyZeq7kpXNqW/QIb8WxLNyuEILUF8.zXJyi', NULL, '2022-05-11', '+221 77 224 68 65', NULL, '2022-05-14 19:27:04', '2022-05-14 19:27:04', NULL),
(26, 'Anastasia', 'Sylva', 0, 'cashier', 'anastasia.s@ism.sn', '$2y$10$HJucj7Y3YMgane0ocBTnQuXZ1nACEerTq13NkjJiifq4.cGg74/by', NULL, '2022-05-03', '+221 77 000 00 00', NULL, '2022-05-14 21:53:28', '2022-05-14 21:53:28', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vigils`
--

CREATE TABLE `vigils` (
  `id` int(11) NOT NULL,
  `user_id` int(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vigils`
--

INSERT INTO `vigils` (`id`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 24, '2022-05-14 19:28:17', '2022-05-14 19:28:17', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cashiers`
--
ALTER TABLE `cashiers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `professors`
--
ALTER TABLE `professors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vigils`
--
ALTER TABLE `vigils`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cashiers`
--
ALTER TABLE `cashiers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `notes`
--
ALTER TABLE `notes`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `professors`
--
ALTER TABLE `professors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `vigils`
--
ALTER TABLE `vigils`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
