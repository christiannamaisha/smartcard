<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->middleware('auth')->name('home');


Route::post('/users', [App\Http\Controllers\UserController::class, 'store'])->middleware('auth')->middleware('is_admin')->name('users.store');


Route::resource('students', App\Http\Controllers\StudentController::class)->middleware('auth')->middleware('is_admin');


Route::resource('cashiers', App\Http\Controllers\CashierController::class)->middleware('auth')->middleware('is_admin');


Route::resource('classes', App\Http\Controllers\ClassController::class)->middleware('auth')->middleware('is_admin');


Route::resource('courses', App\Http\Controllers\CourseController::class)->middleware('auth')->middleware('is_admin');


Route::resource('notes', App\Http\Controllers\NoteController::class)->middleware('auth')->middleware('is_admin');


Route::resource('professors', App\Http\Controllers\ProfessorController::class)->middleware('auth');


Route::resource('vigils', App\Http\Controllers\VigilController::class)->middleware('auth')->middleware('is_admin');


Route::resource('payments', App\Http\Controllers\PaymentController::class);


//Qr code 
Route::get('/qrcode', 'App\Http\Controllers\QRcodeController@index')->middleware('auth')->middleware('is_admin')->name('qrcode.index');
Route::get('/qrcode_parse/{id}', 'App\Http\Controllers\QRcodeController@read')->name('qrcode.read');
