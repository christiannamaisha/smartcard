@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Liste des étudiants</h1>
                </div>
                
            </div>
        </div>
    </section>

    <div class="content px-3">

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="card">
            <div class="card-body p-0">
                <div class="table-responsive">
                    @if(isset($students[0]))
                        <table class="table" id="courses-table">
                            <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Prenom</th>
                                <th>Numero</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($students as $student)

                                <tr>
                                    <td>{{ $student->nom }}</td>
                                    <td>{{ $student->prenom }}</td>
                                    <td>{{ $student->numero }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <br>
                        <center><h5>Aucune éléve dans cette classe pour l'instant 😊. </h5></center>
                        <br>
                    @endif
                </div>
                <div class="card-footer clearfix">
                    <div class="float-right">
                        
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection