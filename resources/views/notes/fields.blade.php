@php
$courses = App\Models\Course::all()->pluck('libelle', 'id');
$students = App\Models\User::where('profile','student')->pluck('nom', 'id');

@endphp
<!-- Course Field -->
<div class="form-group col-sm-6">
    {!! Form::label('course_id', 'Course :') !!}
    {!! Form::select('course_id', $courses , null, ['placeholder' => 'Affecter une classe ...', 'class' => 'form-control']) !!}
</div>
<!-- Student Field -->
<div class="form-group col-sm-6">
    {!! Form::label('etudiant_id', 'Student :') !!}
    {!! Form::select('etudiant_id', $students , null, ['placeholder' => 'Affecter un etudiant ...', 'class' => 'form-control']) !!}
</div>

<!-- Valeur Field -->
<div class="form-group col-sm-6">
    {!! Form::label('valeur', 'Valeur:') !!}
    {!! Form::number('valeur', null, ['placeholder' => 'Donner une note sur 20', 'class' => 'form-control']) !!}
</div>