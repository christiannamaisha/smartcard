<div class="table-responsive">
    <table class="table" id="notes-table">
        <thead>
        <tr>
            <th>Course</th>
            <th>Student</th>
            <th>Valeur</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($notes as $note)
            @php
                $student = App\Models\User::find($note->etudiant_id);
                $course = App\Models\Course::find($note->course_id);
            @endphp
            <tr>
                <td>{{ $course->libelle }}</td>
                <td>{{ $student->nom }} {{ $student->prenom }}</td>
                <td>{{ $note->valeur }}/20</td>
                <td width="120">
                    {!! Form::open(['route' => ['notes.destroy', $note->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('notes.show', [$note->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('notes.edit', [$note->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
