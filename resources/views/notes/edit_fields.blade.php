<!-- Valeur Field -->
<div class="form-group col-sm-6">
    {!! Form::label('valeur', 'Valeur:') !!}
    {!! Form::number('valeur', null, ['placeholder' => 'Donner une note sur 20', 'class' => 'form-control']) !!}
</div>