
<!-- Nom Field -->
<div class="col-sm-12">
    {!! Form::label('nom', 'Nom :') !!}
    <p>{{ $professor->nom }}</p>
</div>


<!-- Prenom Field -->
<div class="col-sm-12">
    {!! Form::label('prenom', 'Prenom :') !!}
    <p>{{ $professor->prenom }}</p>
</div>

<!-- Email Field -->
<div class="col-sm-12">
    {!! Form::label('email', 'Email :') !!}
    <p>{{ $professor->email }}</p>
</div>


<!-- Naisance Field -->
<div class="col-sm-12">
    {!! Form::label('date_naissance', 'Date naissance :') !!}
    <p>{{ $professor->date_naissance }}</p>
</div>

<!-- Numero Field -->
<div class="col-sm-12">
    {!! Form::label('numero', 'Numero :') !!}
    <p>{{ $professor->numero }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $professor->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $professor->updated_at }}</p>
</div>

