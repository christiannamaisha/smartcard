<div class="table-responsive">
    <table class="table" id="professors-table">
        <thead>
        <tr>
            <th>Nom</th>
            <th>Prenom</th>
            <th>Email</th>
            <th>Numero</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($professors as $professor)
            <tr>
                <td>{{ $professor->nom }}</td>
                <td>{{ $professor->prenom }}</td>
                <td>{{ $professor->email }}</td>
                <td>{{ $professor->numero }}</td>                
                <td width="120">
                    {!! Form::open(['route' => ['professors.destroy', $professor->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('professors.show', [$professor->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('professors.edit', [$professor->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
