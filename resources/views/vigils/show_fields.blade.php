
<!-- Nom Field -->
<div class="col-sm-12">
    {!! Form::label('nom', 'Nom :') !!}
    <p>{{ $vigil->nom }}</p>
</div>


<!-- Prenom Field -->
<div class="col-sm-12">
    {!! Form::label('prenom', 'Prenom :') !!}
    <p>{{ $vigil->prenom }}</p>
</div>

<!-- Email Field -->
<div class="col-sm-12">
    {!! Form::label('email', 'Email :') !!}
    <p>{{ $vigil->email }}</p>
</div>


<!-- Naisance Field -->
<div class="col-sm-12">
    {!! Form::label('date_naissance', 'Date naissance :') !!}
    <p>{{ $vigil->date_naissance }}</p>
</div> 

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $vigil->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $vigil->updated_at }}</p>
</div>

