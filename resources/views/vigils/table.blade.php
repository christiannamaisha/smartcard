<div class="table-responsive">
    <table class="table" id="vigils-table">
        <thead>
        <tr>
            <th>Nom</th>
            <th>Prenom</th>
            <th>Email</th>
            <th>Numero</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($vigils as $vigil)
            <tr>
                <td>{{ $vigil->nom }}</td>
                <td>{{ $vigil->prenom }}</td>
                <td>{{ $vigil->email }}</td>
                <td>{{ $vigil->numero }}</td>                           <td width="120">
                    {!! Form::open(['route' => ['vigils.destroy', $vigil->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('vigils.show', [$vigil->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('vigils.edit', [$vigil->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
