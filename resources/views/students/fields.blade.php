<!-- Groupe Sanguin Field -->
<div class="form-group col-sm-6">
    {!! Form::label('groupe_sanguin', 'Groupe Sanguin :') !!}
    {!! Form::select('groupe_sanguin', ['O+' => 'O+', 'O-' => 'O-', 'A-' => 'A-', 'A+' => 'A+', 'B+' => 'B+', 'B-' => 'B-', 'AB+' => 'AB+', 'AB-' => 'AB-' ], null, ['placeholder' => 'Préciser le groupe sanguin ...', 'class' => 'form-control']) !!}
</div>

<!-- Allergie Field -->
<div class="form-group col-sm-6">
    {!! Form::label('allergie', 'Allergie:') !!}
    {!! Form::text('allergie', null, ['class' => 'form-control']) !!}
</div>

<!-- Maladie Recurrente Field -->
<div class="form-group col-sm-6">
    {!! Form::label('maladie_recurrente', 'Maladie Recurrente :') !!}
    {!! Form::text('maladie_recurrente', null, ['class' => 'form-control']) !!}
</div>

<!-- Classe Field -->
<div class="form-group col-sm-6">
    {!! Form::label('classe_id', 'Classe :') !!}
    {!! Form::select('classe_id', $classes , null, ['placeholder' => 'Affecter une classe ...', 'class' => 'form-control']) !!}
</div>

<input type="hidden" name="profile" value="student">