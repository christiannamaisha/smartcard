<div class="table-responsive">
    <table class="table" id="students-table">
        <thead>
        <tr>
            <th></th>
            <th>Nom</th>
            <th>Prenom</th>
            <th>Email</th>
            <th>Numero</th>
            <th>Groupe Sanguin</th>
            <th>Allergie</th>
            <th>Maladie Recurrente</th>
            <th>Classe</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($students as $student)
            @php
                $classe = App\Models\Classe::find($student->classe_id);
                if($student->photo)
                    $url = Storage::disk('s3')->temporaryUrl('users/'. $student->photo, now()->addMinutes(10)); 
                else
                    $url = asset('/assets/img/messages-3.jpg');

            @endphp
            <tr>
                <td><img src="{{ $url }}" width="30" alt="photo illustrative user" ></td>
                <td>{{ $student->nom }}</td>
                <td>{{ $student->prenom }}</td>
                <td>{{ $student->email }}</td>
                <td>{{ $student->numero }}</td>
                <td>{{ $student->groupe_sanguin }}</td>
                <td>{{ $student->allergie }}</td>
                <td>{{ $student->maladie_recurrente }}</td>
                <td>{{ $classe->libelle }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['students.destroy', $student->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('students.show', [$student->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('students.edit', [$student->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>



