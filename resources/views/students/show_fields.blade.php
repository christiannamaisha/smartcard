@php
$classe = App\Models\Classe::find($student->classe_id);
@endphp

<!-- Nom Field -->
<!-- <div class="col-sm-12">
    {!! Form::label('nom', 'Nom :') !!}
    <p>{{ $student->nom }}</p>
</div>-->


<!-- Prenom Field -->
<!-- <div class="col-sm-12">
    {!! Form::label('prenom', 'Prenom :') !!}
    <p>{{ $student->prenom }}</p>
</div> -->

<!-- Email Field -->
<!-- <div class="col-sm-12">
    {!! Form::label('email', 'Email :') !!}
    <p>{{ $student->email }}</p>
</div> -->


<!-- Naisance Field -->
<!-- <div class="col-sm-12">
    {!! Form::label('date_naissance', 'Date naissance :') !!}
    <p>{{ $student->date_naissance }}</p>
</div> -->

<!-- Numero Field -->
<!-- <div class="col-sm-12">
    {!! Form::label('numero', 'Numero :') !!}
    <p>{{ $student->numero }}</p>
</div> -->

<!-- Groupe Sanguin Field -->
<!-- <div class="col-sm-12">
    {!! Form::label('groupe_sanguin', 'Groupe Sanguin:') !!}
    <p>{{ $student->groupe_sanguin }}</p>
</div> -->

<!-- Allergie Field -->
<!-- <div class="col-sm-12">
    {!! Form::label('allergie', 'Allergie:') !!}
    <p>{{ $student->allergie }}</p>
</div> -->

<!-- Maladie Recurrente Field -->
<!-- <div class="col-sm-12">
    {!! Form::label('maladie_recurrente', 'Maladie Recurrente:') !!}
    <p>{{ $student->maladie_recurrente }}</p>
</div> -->


<!-- Classe Id Field -->
<!-- <div class="col-sm-12">
    {!! Form::label('classe_id', 'Classe Id:') !!}
    <p>{{ $classe->libelle }}</p>
</div> -->

<!-- Created At Field -->
<!-- <div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $student->created_at }}</p>
</div> -->

<!-- Updated At Field -->
<!-- <div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $student->updated_at }}</p>
</div> --> 

@include('users.head')

    <section class="section profile">
      <div class="row">
        <div class="col-xl-4">

          <div class="card">
            <div class="card-body profile-card pt-4 d-flex flex-column align-items-center">

              <img src="{{ url('images/', Auth::user()->photo) }}" alt="Profile" class="rounded-circle">
              <h2>{{ $student->nom }}</h2>
              <h3>{{ $classe->libelle }}</h3>
              
            </div>
          </div>

        </div>

        <div class="col-xl-8">

          <div class="card">
            <div class="card-body pt-3">
              <!-- Bordered Tabs -->
              <ul class="nav nav-tabs nav-tabs-bordered">

                <li class="nav-item">
                  <button class="nav-link active" data-bs-toggle="tab" data-bs-target="#profile-overview">A propos</button>
                </li>

                <li class="nav-item">
                  <button class="nav-link" data-bs-toggle="tab" data-bs-target="#profile-edit">Edit Profile</button>
                </li>

                <li class="nav-item">
                  <button class="nav-link" data-bs-toggle="tab" data-bs-target="#profile-change-password">Change Password</button>
                </li>

              </ul>
              <div class="tab-content pt-2">

                <div class="tab-pane fade show active profile-overview" id="profile-overview">
                  

                  <div class="row">
                  
                    <div class="col-lg-3 col-md-4 label "> Nom</div>
                    <div class="col-lg-9 col-md-8">{{ $student->prenom }} {{ $student->nom }} </div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label">Classe</div>
                    <div class="col-lg-9 col-md-8">{{ $classe->libelle }}</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label">Alergie</div>
                    <div class="col-lg-9 col-md-8">{{ $student->allergie }}</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label">Groupe sanguin</div>
                    <div class="col-lg-9 col-md-8">{{ $student->groupe_sanguin }}+</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label">Maladie récurente</div>
                    <div class="col-lg-9 col-md-8">Crise cardiaque</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label">Phone</div>
                    <div class="col-lg-9 col-md-8">{{ $student->numero }}</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label">Email</div>
                    <div class="col-lg-9 col-md-8">{{ $student->email }}</div>
                  </div>

                </div>

                <div class="tab-pane fade profile-edit pt-3" id="profile-edit">

                  <!-- Profile Edit Form -->
                  <form>
                    <div class="row mb-3">
                      <label for="profileImage" class="col-md-4 col-lg-3 col-form-label">Profile Image</label>
                      <div class="col-md-8 col-lg-9">
                        <img src="assets/img/profile-img.jpg" alt="Profile">
                        <div class="pt-2">
                          <a href="#" class="btn btn-primary btn-sm" title="Upload new profile image"><i class="bi bi-upload"></i></a>
                          <a href="#" class="btn btn-danger btn-sm" title="Remove my profile image"><i class="bi bi-trash"></i></a>
                        </div>
                      </div>
                    </div>
                    @include('adminlte-templates::common.errors')
                    {!! Form::model($student, ['route' => ['students.update', $student->id], 'method' => 'patch']) !!}
                    <div class="row mb-3">
                      <label for="fullName" class="col-md-4 col-lg-3 col-form-label">Nom</label>
                      <div class="col-md-8 col-lg-9">
                        <input name="fullName" type="text" class="form-control" id="fullName" value="{{ $student->nom }}">
                      </div>
                    </div>

                    

                    <div class="row mb-3">
                      <label for="company" class="col-md-4 col-lg-3 col-form-label">Classe</label>
                      <div class="col-md-8 col-lg-9">
                        <input name="company" type="text" class="form-control" id="company" value="L2">
                      </div>
                    </div>

                    <div class="row mb-3">
                      <label for="Job" class="col-md-4 col-lg-3 col-form-label">Alergie</label>
                      <div class="col-md-8 col-lg-9">
                        <input name="job" type="text" class="form-control" id="Job" value="{{ $student->allergie }}">
                      </div>
                    </div>

                    <div class="row mb-3">
                      <label for="Country" class="col-md-4 col-lg-3 col-form-label">Groupe sanguin</label>
                      <div class="col-md-8 col-lg-9">
                        <input name="country" type="text" class="form-control" id="Country" value="{{ $student->groupe_sanguin }}">
                      </div>
                    </div>

                    <div class="row mb-3">
                      <label for="Address" class="col-md-4 col-lg-3 col-form-label">Maladie recurente</label>
                      <div class="col-md-8 col-lg-9">
                        <input name="address" type="text" class="form-control" id="Address" value="{{ $student->maladie_recurrente }}">
                      </div>
                    </div>

                    <div class="row mb-3">
                      <label for="Phone" class="col-md-4 col-lg-3 col-form-label">Phone</label>
                      <div class="col-md-8 col-lg-9">
                        <input name="phone" type="text" class="form-control" id="Phone" value="{{ $student->numero }}">
                      </div>
                    </div>

                    <div class="row mb-3">
                      <label for="Email" class="col-md-4 col-lg-3 col-form-label">Email</label>
                      <div class="col-md-8 col-lg-9">
                        <input name="email" type="email" class="form-control" id="Email" value="{{ $student->email }}">
                      </div>
                    </div>


                    <div class="text-center">
                    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                    </div>
                  </form><!-- End Profile Edit Form -->

                </div>

                

                <div class="tab-pane fade pt-3" id="profile-change-password">
                  <!-- Change Password Form -->
                  <form>

                    <div class="row mb-3">
                      <label for="currentPassword" class="col-md-4 col-lg-3 col-form-label">Current Password</label>
                      <div class="col-md-8 col-lg-9">
                        <input name="password" type="password" class="form-control" id="currentPassword">
                      </div>
                    </div>

                    <div class="row mb-3">
                      <label for="newPassword" class="col-md-4 col-lg-3 col-form-label">New Password</label>
                      <div class="col-md-8 col-lg-9">
                        <input name="newpassword" type="password" class="form-control" id="newPassword">
                      </div>
                    </div>

                    <div class="row mb-3">
                      <label for="renewPassword" class="col-md-4 col-lg-3 col-form-label">Re-enter New Password</label>
                      <div class="col-md-8 col-lg-9">
                        <input name="renewpassword" type="password" class="form-control" id="renewPassword">
                      </div>
                    </div>

                    <div class="text-center">
                      <button type="submit" class="btn btn-primary">Change Password</button>
                    </div>
                  </form><!-- End Change Password Form -->

                </div>

              </div><!-- End Bordered Tabs -->

            </div>
          </div>

        </div>
      </div>
    </section>

<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{asset('assets/vendor/apexcharts/apexcharts.min.js')}}"></script>
  <script src="{{asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('assets/vendor/chart.js/chart.min.js')}}"></script>
  <script src="{{asset('assets/vendor/echarts/echarts.min.js')}}"></script>
  <script src="{{asset('assets/vendor/quill/quill.min.js')}}"></script>
  <script src="{{asset('assets/vendor/simple-datatables/simple-datatables.js')}}"></script>
  <script src="{{asset('assets/vendor/tinymce/tinymce.min.js')}}"></script>
  <script src="{{asset('assets/vendor/php-email-form/validate.js')}}"></script>

  <!-- Template Main JS File -->
  <script src="{{asset('assets/js/main.js')}}"></script>