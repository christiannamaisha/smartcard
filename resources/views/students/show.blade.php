@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Student Details</h1>
                </div>
                <div class="col-sm-6">
                    <a class="btn btn-default float-right"
                       href="{{ route('students.index') }}">
                        Back
                    </a>
                </div>
            </div>
        </div>
    </section>

    
     @include('students.show_fields')
              
@endsection
