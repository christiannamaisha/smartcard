<!-- Nom Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nom', 'Nom :') !!}
    {!! Form::text('nom', null, ['class' => 'form-control']) !!}
</div>

<!-- Prenom Field -->
<div class="form-group col-sm-6">
    {!! Form::label('prenom', 'Prenom :') !!}
    {!! Form::text('prenom', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email :') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>


<!-- Date de naissance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_naissance', 'Date de naissance :') !!}
    {!! Form::date('date_naissance', null, ['class' => 'form-control']) !!}
</div>

<!-- Numero Field -->
<div class="form-group col-sm-12">
    {!! Form::label('numero', 'Numéro :') !!}
    {!! Form::text('numero', null, ['placeholder'=>'+221 77 *** ** **', 'class' => 'form-control']) !!}
</div>

<!--Photo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('photo', 'Photo :') !!}
    {!! Form::file('photo', ['class' => 'form-control']) !!}
</div>