<!-- Nom Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nom', 'Nom :') !!}
    {!! Form::text('nom', null, ['class' => 'form-control']) !!}
</div>

<!-- Prenom Field -->
<div class="form-group col-sm-6">
    {!! Form::label('prenom', 'Prenom :') !!}
    {!! Form::text('prenom', null, ['class' => 'form-control']) !!}
</div>


<!-- Profile Field -->
<div class="form-group col-sm-6">
    {!! Form::label('photo', 'Photo de profil :') !!}
    {!! Form::file('photo')       !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email :') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>


<!-- Date de naissance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_naissance', 'Date de naissance :') !!}
    {!! Form::date('date_naissance', null, ['class' => 'form-control']) !!}
</div>

<!-- Numero Field -->
<div class="form-group col-sm-6">
    {!! Form::label('numero', 'Numéro :') !!}
    {!! Form::text('numero', null, ['placeholder'=>'+221 77 *** ** **', 'class' => 'form-control']) !!}
</div>

<!-- Mot de passe Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', 'Mot de passe :') !!}
    {!! Form::password('password', ['placeholder'=>'Password', 'class' => 'form-control']) !!}
</div>
