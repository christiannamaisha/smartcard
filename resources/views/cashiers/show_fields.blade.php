
<!-- Nom Field -->
<div class="col-sm-12">
    {!! Form::label('nom', 'Nom :') !!}
    <p>{{ $cashier->nom }}</p>
</div>


<!-- Prenom Field -->
<div class="col-sm-12">
    {!! Form::label('prenom', 'Prenom :') !!}
    <p>{{ $cashier->prenom }}</p>
</div>

<!-- Email Field -->
<div class="col-sm-12">
    {!! Form::label('email', 'Email :') !!}
    <p>{{ $cashier->email }}</p>
</div>


<!-- Naisance Field -->
<div class="col-sm-12">
    {!! Form::label('date_naissance', 'Date naissance :') !!}
    <p>{{ $cashier->date_naissance }}</p>
</div>

<!-- Numero Field -->
<div class="col-sm-12">
    {!! Form::label('numero', 'Numero :') !!}
    <p>{{ $cashier->numero }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $cashier->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $cashier->updated_at }}</p>
</div>

