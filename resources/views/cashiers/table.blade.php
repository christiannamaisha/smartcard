<div class="table-responsive">
    <table class="table" id="cashiers-table">
        <thead>
        <tr>
            <th>Nom</th>
            <th>Prenom</th>
            <th>Email</th>
            <th>Numero</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($cashiers as $cashier)
            <tr>
                <td>{{ $cashier->nom }}</td>
                <td>{{ $cashier->prenom }}</td>
                <td>{{ $cashier->email }}</td>
                <td>{{ $cashier->numero }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['cashiers.destroy', $cashier->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('cashiers.show', [$cashier->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('cashiers.edit', [$cashier->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
