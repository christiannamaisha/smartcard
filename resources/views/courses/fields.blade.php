
<!-- Libelle Field -->
<div class="form-group col-sm-6">
    {!! Form::label('libelle', 'Libelle :') !!}
    {!! Form::text('libelle', null, ['class' => 'form-control']) !!}
</div>

<!-- Professor Field -->
<div class="form-group col-sm-6">
    {!! Form::label('professor_id', 'Professor :') !!}
    {!! Form::select('professor_id', $professors , null, ['placeholder' => 'Affecter un professeur ...', 'class' => 'form-control']) !!}
</div>