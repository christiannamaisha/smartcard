@php
$professor = App\Models\User::find($course->professor_id);
@endphp

<!-- Libelle Field -->
<div class="col-sm-12">
    {!! Form::label('libelle', 'Libelle :') !!}
    <p>{{ $course->libelle }}</p>
</div>

<!-- Professor Id Field -->
<div class="col-sm-12">
    {!! Form::label('professeur', 'Professeur :') !!}
    <p>{{ $professor->nom }} {{ $professor->prenom }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $course->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $course->updated_at }}</p>
</div>

