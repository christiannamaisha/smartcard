@php
if ($student->classe_id){
    $classe = App\Models\Classe::find($student->classe_id);
    $classe_defined = "de la classe <b>" . $classe->libelle . "</b>" ;
}
else 
    $classe_defined = "d'aucune classe 'LOL on dirait un fantôme 😅'";

if($student->photo)
    $url = Storage::disk('s3')->temporaryUrl('users/'. $student->photo, now()->addMinutes(10)); 
else
    $url = asset('/assets/img/messages-3.jpg');


@endphp

<!-- Nom Field -->
<div class="col-sm-12"> 
    <h3><img src="{{ $url }}" class="rounded" alt="photo illustrative user"><b>{{ $student->nom }} {{ $student->prenom }} </b> {!! $classe_defined !!} </h3>
</div>

<!-- Naisance Field -->
<div class="col-sm-12">
    {!! Form::label('date_naissance', 'Date naissance :') !!}
    <p>{{ $student->date_naissance }}</p>
</div>

<!-- Numero Field -->
<div class="col-sm-12">
    {!! Form::label('numero', 'Numero :') !!}
    <p>{{ $student->numero }}</p>
</div>

<!-- Groupe Sanguin Field -->
<div class="col-sm-12">
    {!! Form::label('groupe_sanguin', 'Groupe Sanguin:') !!}
    <p>{{ $student->groupe_sanguin }}</p>
</div>

<!-- Allergie Field -->
<div class="col-sm-12">
    {!! Form::label('allergie', 'Allergie:') !!}
    <p>{{ $student->allergie }}</p>
</div>

<!-- Maladie Recurrente Field -->
<div class="col-sm-12">
    {!! Form::label('maladie_recurrente', 'Maladie Recurrente:') !!}
    <p>{{ $student->maladie_recurrente }}</p>
</div>


<!-- Payments Field -->
<div class="col-sm-12">
    @if(!empty($payments))
        {!! Form::label('payment', 'Mensualité payés par cette étudiant') !!}
        <p>
        @foreach ($payments as $item)
            {{ $item->month }}, 
        @endforeach
        </p>
    @else
        <p>Aucune mensualité assuré par cette étudiant "Un peu dessus"</p>
    @endif
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $student->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $student->updated_at }}</p>
</div>

