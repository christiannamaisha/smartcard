@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>ETUDIANT - SUPER HEROES 🦸🏾‍♂️ </h1>
                </div>
                
            </div>
        </div>
    </section>


    <div class="content px-3">

        <div class="clearfix"></div>

        <div class="card">
            <div class="card-body p-0">
                <div class="table-responsive">
                    @if(isset($students[0]))
                        <table class="table" id="courses-table">
                            <thead>
                            <tr>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($students as $student)
                                @php
                                    $data = Crypt::encryptString($student->id);
                                    $path = route('qrcode.read',[$data]);
                                @endphp
                                <tr>
                                    <td>{{ \QrCode::size(100)->generate($path) }}</td>
                                    <td>{{ $student->nom }} {{ $student->prenom }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <br>
                        <center><h5>Pas d'étudiant dans toute votre école, Veuillez fermer ! 😊.</h5></center>
                        <br>
                    @endif
                </div>
                <div class="card-footer clearfix">
                    <div class="float-right">
                        
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
