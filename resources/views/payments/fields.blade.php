
@php
$months = ['Janvier' => 'Janvier', 'Février' => 'Février', 'Mars' => 'Mars', 'Avril' => 'Avril', 'Mai' => 'Mai', 'Juin' => 'Juin', 'Juillet' => 'Juillet', 'Aôut' => 'Aôut', 'Septembre' => 'Septembre', 'Octobre' => 'Octobre', 'Novembre' => 'Novembre', 'Décembre'=>'Décembre' ];
@endphp

<!-- Month Field -->
<div class="form-group col-sm-6">
    {!! Form::label('month', 'Mois ?') !!}
    {!! Form::select('month', $months, null, ['placeholder' => 'Sélectionner le mois ...', 'class' => 'form-control']) !!}
</div>

<!-- Student Field -->
<div class="form-group col-sm-6">
    {!! Form::label('student_id', 'Student ? ') !!}
    {!! Form::select('student_id', $students , null, ['placeholder' => 'Sélectionner l\'étudiant ... ', 'class' => 'form-control']) !!}
</div>


