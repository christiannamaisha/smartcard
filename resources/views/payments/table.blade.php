<div class="table-responsive">
    <table class="table" id="payments-table">
        <thead>
        <tr>
            <th>Month</th>
            <th>Student</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($payments as $payment)
            @php
            $student = App\Models\User::find($payment->student_id);
            @endphp
            <tr>
                <td>{{ $payment->month }}</td>
                <td>{{ $student->nom }} {{ $student->prenom }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['payments.destroy', $payment->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('payments.show', [$payment->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('payments.edit', [$payment->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
       
        </tbody>
    </table>
</div>
