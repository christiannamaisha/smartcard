<li class="nav-item">
    <a href="{{ route('students.index') }}"
       class="nav-link {{ Request::is('students*') ? 'active' : '' }}">
        <p>Students</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('cashiers.index') }}"
       class="nav-link {{ Request::is('cashiers*') ? 'active' : '' }}">
        <p>Cashiers</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('classes.index') }}"
       class="nav-link {{ Request::is('classes*') ? 'active' : '' }}">
        <p>Classes</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('courses.index') }}"
       class="nav-link {{ Request::is('courses*') ? 'active' : '' }}">
        <p>Courses</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('notes.index') }}"
       class="nav-link {{ Request::is('notes*') ? 'active' : '' }}">
        <p>Notes</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('professors.index') }}"
       class="nav-link {{ Request::is('professors*') ? 'active' : '' }}">
        <p>Professors</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('vigils.index') }}"
       class="nav-link {{ Request::is('vigils*') ? 'active' : '' }}">
        <p>Vigils</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('payments.index') }}"
       class="nav-link {{ Request::is('payments*') ? 'active' : '' }}">
        <p>Payments</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('qrcode.index') }}"
       class="nav-link {{ Request::is('qrcode*') ? 'active' : '' }}">
        <p>HEROES</p>
    </a>
</li>


